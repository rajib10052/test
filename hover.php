

<!DOCTYPE html> 
<html > 
<head> 
<style type="text/css"> 
	.box { 
		width: 150px; 
		border: 1px solid #9325BC; 
		padding: 10px; 
	} 
	.box:hover { 
		-moz-box-shadow: 0 0 10px #ccc; 
		-webkit-box-shadow: 0 0 10px #ccc; 
		box-shadow: 0 0 10px #ccc; 
	} 
</style> 
</head> 
<body> 
<div class="box"> 
Move your mouse here 
</div> 
<a
   href="abc.html"
   onMouseOver="this.style.color='#0F0'"
   onMouseOut="this.style.color='#00F'"
>Text</a>
</body> 
</html> <!-- See more at: http://www.corelangs.com/css/box/hover.html#sthash.6vLIajJU.dpuf-->