<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>serialize demo</title>
  <style>
  body, select {
    font-size: 12px;
  }
  form {
    margin: 5px;
  }
  p {
    color: red;
    margin: 5px;
    font-size: 14px;
  }
  b {
    color: blue;
  }
  </style>
  <script src="//code.jquery.com/jquery-1.10.2.js"></script>
</head>
<body>
 
<form>
  
  <input type="hidden" name="check[]" value="uttam" id="ch1">
  <input type="hidden" name="check[]" value="kumar" id="ch1">
  <input type="hidden" name="check[]" value="roy" id="ch1">
  
</form>
 
<p><tt id="results"></tt></p>
 
<script>
  function showValues() {
    var str = $( "form" ).serialize();
    $( "#results" ).text( str );
  }
  
  showValues();
</script>
 
</body>
</html>