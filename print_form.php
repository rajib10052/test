<!DOCTYPE html>
<html>
<head>
<style>
fieldset {
 border:1px solid green;
 min-width:50%;
    margin: 0.25em 2em 0.25em 1em !important;
    -webkit-box-shadow:  0px 0px 0px 0px #000;
            box-shadow:  0px 0px 0px 0px #000;
	-webkit-border-radius: 8px;
    -moz-border-radius: 8px;
     border-radius: 5px; 
	 }

  .control-label {
    padding-top: 0.5px;
    margin-bottom: 0;
    text-align: right;
  }
  .form-group {
  margin-bottom: 5px;
}
legend {
  padding: 0.2em 0.5em;
  border:1px solid green;
  color:green;
  font-size:90%;
  text-align:right;
  
  }

  label {
 
  float:center;
  width:25%;
  margin-right:0.5em;
  margin-bottom:0.5em;
  padding-top:0.2em;
  text-align:right;
  }
  .form-control {
  height: 18px;
  background-color: #fff;
  color: #555;
  padding: 6px 12px;
  line-height: 1.42857143;
  border: 1px solid #ccc;
  background-image: none;
  border-radius: 4px;
    -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
          box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
		    -webkit-transition: border-color ease-in-out .15s, -webkit-box-shadow ease-in-out .15s;
       -o-transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
          transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
}
</style>
</head>
<body>
<div id="printID">
<form>
  <fieldset>
  <legend>Patient info</legend>

      <div class="form-group"><label for="inputName" class="control-label" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Name:</label><input type="name" style="width:40%"  class="form-control" name="inputName" ></div>
	 <div class="form-group"> <label for="Sex"  class="control-label" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Sex:</label><input type="Sex" style="width:20%" class="form-control" name="Sex" >
     <label for="inputAge">Age:</label><input type="Age" style="width:20%" class="form-control" name="inputAge" ></div>
	<div class="form-group">  <label for="BirthDay">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Birth Day:</label><input type="text" style="width:40%" class="form-control" name="BirthDay" ></div>
	 <div class="form-group"> <label for="Blood">Blood Group:</label><input type="Blood" style="width:25%" class="form-control" name="Blood" ></div>
   <div class="form-group"> <label for="Mobile">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Mobile:</label><input type="Mobile" style="width:30%" class="form-control" name="Mobile" ></div>
   <div class="form-group">   <label for="Phone">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Phone:</label><input type="Phone" style="width:30%" class="form-control" name="Phone" ></div>
    <div class="form-group"> <label for="Email">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Email:</label><input type="Email" style="width:40%" class="form-control" name="Email" ></div>
	<div class="form-group"> <label for="Date">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Date:</label><input type="text" style="width:40%" class="form-control" name="Date" ></div>
   <div class="form-group"> <label for="Address">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Address:</label><textarea rows="2" cols="65" name="Address">
		
		</textarea></div>
    <label for="clinicName">Clinic Name:</label><input type="name" style="width:40%"  class="form-control" name="clinicName" >
</fieldset>
  <fieldset>
  <legend>History</legend>
  <label for="Drug">Drug Allergy:</label><input rows="1" cols="25" name="Drug"></input>
	Yes&nbsp;&nbsp;No<input type="checkbox" name="vehicle" value="Bike"/></br>
  <label for="Hypertension">Hypertension:</label><input rows="1" cols="25" name="Hypertension"></input>
	Yes&nbsp;&nbsp;No<input type="checkbox" name="vehicle" value="Bike"/></br>
  <label for="Diabetes">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Diabetes:</label><input rows="1" cols="60" name="Diabetes"></input>
	Yes&nbsp;&nbsp;No<input type="checkbox" name="vehicle" value="Bike"/></br>
  <label for="Other">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Others:</label><input rows="1" cols="60" name="Other"></input>
	Yes&nbsp;&nbsp;No<input type="checkbox" name="vehicle" value="Bike"/></br>
  </fieldset>
  <p align="middle">
	Consent: I, the undersign Mr/Mrs ......................... has given my consent for the dental procedure being performed for</br>
	myself/my ...................... under any anaesthesia demand suitable. I was explained the risk involved in the procedure. </br>
	I was also explained about the costs and agreed to pay.</br>
	<table align="">
	<tr>
		<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;......................................................</td>
		<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;...................................................</td>
	</tr>
	<tr>
		<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Signature O.P.D. Incharge</td>
		<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Signature of Patient/Attendance</td>
	</tr>
  </table>
  </p>
  
          <!--<input style="margin-left:75%" type="submit" value="Print">-->
</form>
</div>
<input style="margin-left:70%" type="button" onclick="printDiv('printID')" value="Print">
<script>
function printDiv(divID) {
            //Get the HTML of div
            var divElements = document.getElementById(divID).innerHTML;
            //Get the HTML of whole page
            var oldPage = document.body.innerHTML;

            //Reset the page's HTML with div's HTML only
            document.body.innerHTML = 
              "<html><head><title></title></head><body>" + 
              divElements + "</body>";

            //Print Page
            window.print();

            //Restore orignal HTML
            document.body.innerHTML = oldPage;

          
        }
</script>
</body>
</html>